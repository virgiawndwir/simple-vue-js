import { createRouter, createWebHistory } from "vue-router";
import MemberIndex from '../views/member/Index.vue';
import MemberCreate from '../views/member/Create.vue';
import MemberEdit from '../views/member/Edit.vue';
import Detail from '../views/member/Detail.vue';
import Register from '../views/Register.vue';
import Login from '../views/Login.vue';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "register",
      component: Register,
    },
    {
      path: "/login",
      name: "login",
      component: Login,
    },
    {
      path: "/member",
      children: [
        {
          path: "/member",
          name: "member.index",
          component: MemberIndex,
        },
        {
          path: "/create",
          name: "member.create",
          component: MemberCreate,
        },
        {
          path: "/edit/:id",
          name: "member.edit",
          component: MemberEdit,
        },
        {
          path: "/detail/:id",
          name: "member.detail",
          component: Detail,
        },
      ]
    },
  ],

});

export default router;
